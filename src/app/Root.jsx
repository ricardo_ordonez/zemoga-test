import React from 'react'
import {BrowserRouter as Router, Route, Switch, Redirect} from "react-router-dom";

// Screens
import HomeScreen from "./home";

// utils
import routes from "./config/routes";

export default function Root() {
  return (
    <Router>
      <Switch>
        <Route exact path={routes.root}>
          <Redirect to={routes.home} />
        </Route>

        <Route exact path={routes.home} component={HomeScreen} />

        <Route exact path={routes.pastTrials} component={() => <h1>Past Trials</h1>} />

        <Route exact path={routes.howItWorks} component={() => <h1>How It Works</h1>} />

        <Route exact path={routes.login} component={() => <h1>Login</h1>} />

        <Route exact path={routes.search} component={() => <h1>Search</h1>} />

        <Route exact path={routes.conditions} component={() => <h1>Terms and Conditions</h1>} />

        <Route exact path={routes.policy} component={() => <h1>Privacy Policy</h1>} />

        <Route exact path={routes.contactUs} component={() => <h1>Contact Us</h1>} />

        <Route component={() => <h1>404</h1>} />
      </Switch>
    </Router>
  )
}
