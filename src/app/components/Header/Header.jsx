/** @jsx jsx */
import { jsx } from "@emotion/core";

// Components
import Menu from "../Menu";

// Styles
import * as styles from "./Header.styles";

export default function Header() {
  return (
    <header css={styles.header}>
      <h1>Rule of Thumb.</h1>

      <Menu />
    </header>
  )
}
