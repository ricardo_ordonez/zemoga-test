import { css } from "@emotion/core";

// Utils
import colors from "../../styles/colors";

export const header = css`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 41px 130px 85px;

  h1 {
    font-weight: 400;
    color: ${colors.white};
    margin: 0;
  }

  @media screen and (max-width: 991px) {
    padding-right: 15px;
    padding-left: 15px;
  }
`;
