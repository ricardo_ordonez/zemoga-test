/** @jsx jsx */
import { jsx } from "@emotion/core";
import { useState } from "react";
import { NavLink } from "react-router-dom";

// Utils
import routes from "../../config/routes";

// Assets
import searchIcon from "../../assets/icons/search.svg";

// Styles
import * as styles from "./Menu.styles";

export default function Menu() {
  const [isMenuOpen, setIsMenuOpen] = useState(false);

  function toggleMenu() {
    setIsMenuOpen(!isMenuOpen);
  }

  return (
    <nav>
      <div css={[styles.hamburger, isMenuOpen && styles.menuOpen]} onClick={toggleMenu} role="button">
        <div className="menu" />
      </div>
      <ul css={[styles.menuList, isMenuOpen && styles.menuOpen]}>
        <li>
          <NavLink to={routes.pastTrials} target="_blank">Past Trials</NavLink>
        </li>

        <li>
          <NavLink to={routes.howItWorks} target="_blank">How It Works</NavLink>
        </li>

        <li>
          <NavLink to={routes.login} target="_blank">Log In / Sign Up</NavLink>
        </li>

        <li>
          <NavLink to={routes.search} target="_blank" className="search">
            <img src={searchIcon} alt="Search" />
          </NavLink>
        </li>
      </ul>
    </nav>
  )
}
