import { css } from "@emotion/core";
import colors from "../../styles/colors";

export const menuList = css`
  display: inline-flex;
  
  li {
    transition: margin 0.2s 0.5s ease;
    &:not(:last-of-type) {
      margin-right: 33px;
    }

    a {
      position: relative;
      padding: 10px;
      color: ${colors.white};
      font-weight: 300;

      &:hover {
        text-decoration: underline;
      }

      &.search {
        img {
          width: 15px;
        }
      }
    }
  }

  @media screen and (max-width: 767px) {
    transform: translateX(100%);
    transition: transform 0.4s 0.1s ease;
    display: flex;
    flex-direction: column;
    position: fixed;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    background-color: ${colors.java};
    overflow-y: auto;
    padding-top: 90px;
    z-index: 90;
  }
`;

export const hamburger = css`
  display: none;
  position: relative;
  width: 40px;
  height: 24px;
  z-index: 91;

  .menu {
    position: absolute;
    width: 40px;
    height: 4px;
    background-color: ${colors.white};
    border-radius: 4px;
    top: 50%;

    &::before,
    &::after {
      content: "";
      position: absolute;
      width: 40px;
      height: 4px;
      background-color: ${colors.white};
      border-radius: 4px;
      transition: transform 0.2s ease-in-out;
    }

    &::before {
      top: -10px;
      width: 20px;
      transform: translateX(20px);
    }

    &::after {
      bottom: -10px;
      width: 30px;
      transform: translateX(10px);
    }
  }

  @media screen and (max-width: 767px) {
    display: inline-flex;
  }
`;

export const menuOpen = css`
  @media screen and (max-width: 767px) {
    transform: translateX(0);
  
    li {
      margin-bottom: 20px;
  
      &:not(:last-of-type) {
        margin-right: 0;
      }


      transition: margin 0.2s 0.5s ease;
    }

    .menu {
      &::before,
      &::after {
        transform: translateX(0);
      }
    }
  }
`;
