/** @jsx jsx */
import { jsx } from "@emotion/core";

// Assets
import upVoteIcon from "../../assets/icons/upvote.svg";
import downVoteIcon from "../../assets/icons/downvote.svg";

// Styles
import * as styles from "./Progress.styles";

export default function Progress({ votes }) {
  function calculatePercentage(number) {
    const totalVotes = votes.up + votes.down;
    const percentage = Math.round((number * 100) / totalVotes);

    return percentage;
  }

  const upVotesPercentage = calculatePercentage(votes.up);
  const downVotesPercentage = calculatePercentage(votes.down);

  return (
    <div css={styles.wrapper}>
      <div css={[styles.progress, styles.progressUp]} style={{width: `${upVotesPercentage}%`}}>
        <img src={upVoteIcon} className="icon" alt="Up vote" />
        <span>{`${upVotesPercentage}%`}</span>
      </div>
      <div css={[styles.progress, styles.progressDown]} style={{width: `${downVotesPercentage}%`}}>
        <span>{`${downVotesPercentage}%`}</span>
        <img src={downVoteIcon} className="icon" alt="Up vote" />
      </div>
    </div>
  )
}
