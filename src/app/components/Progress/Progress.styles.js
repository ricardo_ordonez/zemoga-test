import { css } from "@emotion/core";
import colors from "../../styles/colors";

export const wrapper = css`
  display: flex;
  position: relative;
  height: 49px;
  z-index: 1;
`;

export const progress = css`
  position: relative;
  display: inline-flex;
  align-items: center;
  padding: 10px;
  transition: width 0.3s ease-in;

  &::before {
    content: "";
    position: absolute;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    opacity: 0.7;
    z-index: -1;
  }

  .icon {
    width: 30px;
  }
`;

export const progressUp = css`
  &::before {
    background-color: ${colors.java};
  }

  .icon {
    margin-right: 10px;
  }
`;

export const progressDown = css`
  justify-content: flex-end;

  &::before {
    background-color: ${colors.mySin};
  }

  .icon {
    margin-left: 10px;
  }
`;
