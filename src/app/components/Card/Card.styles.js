import { css } from "@emotion/core";
import { hexToRgb } from "../../lib";
import colors from "../../styles/colors";

export const wrapper = css`
  position: relative;
  display: flex;
  flex-direction: column;
  width: 100%;
  max-width: 494px;
  height: 548px;
  color: ${colors.white};
`;

export const hero = css`
  height: auto;
  overflow: hidden;

  &::before {
    content: "";
    position: absolute;
    width: 100%;
    height: 100%;
    background-color: rgba(${hexToRgb(colors.black)}, 0.34);
  }

  .header-text {
    font-size: 16px;
    font-weight: 300;
    margin: 0;
  }

  .title-text {
    font-size: 60px;
    font-weight: 400;
    margin: 0;
    margin-bottom: 30px;
  }

  .text {
    font-size: 21px;
    font-weight: 300;
    margin: 0;
    margin-bottom: 30px;
  }

  .verdict {
    font-size: 20px;
    margin: 0;
    margin-top: 45px;
  }
`;

export const blurred = css`
  width: 212%;
  height: 100%;
  position: absolute;
  z-index: -1;
  filter: blur(30px);
  left: -1%;
  background-size: cover;
`;

export const content = css`
  position: relative;
  padding: 40px;
  z-index: 1;

  .person-status {
    display: inline-flex;
    align-items: center;
    justify-content: center;
    position: absolute;
    width: 32px;
    height: 32px;
    top: 47px;
    left: 0;
    padding: 0;

    img {
      width: 15px;
    }
  }
`;

export const moreInfo = css`
  color: ${colors.white};
  font-size: 14px;

  img {
    margin-right: 5px;
  }

  a {
    color: inherit;
  }
`;

export const vote = css`
  justify-content: flex-end;
  &::before {
    content: "";
    content: '';
    position: absolute;
    bottom: 0;
    left: 0;
    width: 100%;
    height: 100%;
    background-image: linear-gradient(180deg, rgba(${hexToRgb(colors.black)}, 0) 0%, rgba(${hexToRgb(colors.black)}, 0.5) 100%);
    z-index: 1;
  }

  .bg-image {
    position: absolute;
    width: 100%;
    height: 100%;
    background-size: cover;
    background-position: center;
  }

  .title-text {
    font-size: 45px;
    font-weight: 400;
    margin: 0;
  }

  .time {
    font-size: 12px;
  }

  .summary {
    font-size: 16px;
    font-weight: 300;
    margin-top: 10px;
    margin-bottom: 0;
  }

  .actions {
    display: flex;
    align-items: center;
    margin-top: 30px;

    button {

      &:not(:last-of-type) {
        margin-right: 15px;
      }

      &:focus {
        outline: none;
      }

      &.button-vote {
        padding: 10px;
        font-size: 18px;
      }

      img {
        width: 15px;
      }
    }
  }
`;

export const buttonVoteActive = css`
  border: 2px solid ${colors.white};
`;

export const footer = css`
  position: relative;
`;

export const cardHeroFooter = css`
  display: flex;

  button {
    flex: 1;
    justify-content: center;
    height: 80px;
    opacity: 0.8;
  }
`;
