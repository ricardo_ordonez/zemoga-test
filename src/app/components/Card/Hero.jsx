/** @jsx jsx */
import { jsx } from "@emotion/core";

// Components
import Paragraph from "../Paragraph";
import ButtonVote from "../ButtonVote";

// Assets
import wikiIcon from "../../assets/icons/Wiki.png";

// Styles
import * as styles from "./Card.styles";

export default function Hero(props) {
  return (
    <div className="card" css={[styles.wrapper, styles.hero]}>
        <div style={{backgroundImage: `url(${props.bgImage})`}} css={styles.blurred} />

        <div css={styles.content}>
          <Paragraph tag="h4" className="header-text">What's your opinion on</Paragraph>

          <Paragraph tag="h2" className="title-text">Pope Francis?</Paragraph>

          <Paragraph className="text">
            He's talking on clergy sexual abuse, but is he just another papal pervert
            protector? (thumbs down) or a true pedophile punishing pontiff? (thumbs up)
          </Paragraph>

          <span css={styles.moreInfo}>
            <img src={wikiIcon} alt="Wikipedia" />
            <Paragraph tag="a" href="https://en.wikipedia.org/wiki/Pope_Francis" target="_blank">More information</Paragraph>
          </span>

          <Paragraph tag="h3" className="verdict" css={styles.verdict}>What's Your Verdict?</Paragraph>
        </div>

      <div css={[styles.footer, styles.cardHeroFooter]}>
        <ButtonVote />
        <ButtonVote voteType="downvote" />
      </div>
    </div>
  )
}
