/** @jsx jsx */
import { jsx } from "@emotion/core";
import { Fragment, useState } from "react";
import dayjs from "dayjs";
import relativeTime from "dayjs/plugin/relativeTime";

// Components
import Paragraph from "../Paragraph";
import ButtonVote from "../ButtonVote";
import Button from "../Button";
import ProgressBar from "../Progress";

// Styles
import * as styles from "./Card.styles";

dayjs.extend(relativeTime);

export default function Vote({ person, vote }) {
  const [buttonType, setButtonType] = useState("up");
  const [hasVote, setHasVote] = useState(false);

  function markButtonVote(type) {
    setButtonType(type);
  }

  function handleVote() {
    vote(buttonType, person);
    setHasVote(true);
  }

  function voteAgain() {
    setHasVote(false);
  }

  const isButtonUpMarked = buttonType === "up";
  const isButtonDownMarked = buttonType === "down";

  return (
    <div className="card" css={[styles.wrapper, styles.vote]}>
      <div style={{backgroundImage: `url(${person.avatar})`}} className="bg-image" />

      <div css={styles.content}>
        {
          person.votes.up > person.votes.down ? (
            <ButtonVote className="person-status" />
          ) : (
            <ButtonVote className="person-status" voteType="downvote" />
          )
        }

        <Paragraph tag="h2" className="title-text">{person.name}</Paragraph>

        <time className="time">{dayjs(person.time).fromNow()} in {person.area}</time>

        <Paragraph className="summary">{person.summary}</Paragraph>

        <div className="actions">
          {
            !hasVote ? (
              <Fragment>
                <ButtonVote onClick={() => markButtonVote("up")} css={isButtonUpMarked && styles.buttonVoteActive} />
                <ButtonVote voteType="downvote" onClick={() => markButtonVote("down")} css={isButtonDownMarked && styles.buttonVoteActive} />
                <Button className="button-vote" theme="light" onClick={handleVote}>Vote now</Button>
              </Fragment>
            ) : (
              <Button className="button-vote" theme="light" onClick={voteAgain}>Vote again</Button>
            )
          }
        </div>
      </div>

      <ProgressBar votes={person.votes} />
    </div>
  )
}
