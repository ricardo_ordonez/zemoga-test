/** @jsx jsx */
import { jsx } from "@emotion/core";
import PropTypes from "prop-types";

// Components
import HeroCard from "./Hero";
import VoteCard from "./Vote";

export default function Card({type, ...rest}) {
  return type === "hero" ? <HeroCard {...rest} /> : <VoteCard {...rest} />
}

Card.propTypes = {
  type: PropTypes.string
}

Card.defaultProps = {
  type: "vote"
}
