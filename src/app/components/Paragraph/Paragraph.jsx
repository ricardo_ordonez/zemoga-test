/** @jsx jsx */
import { jsx } from "@emotion/core";
import PropTypes from "prop-types";


export default function Paragraph({tag, children, styleType, ...rest}) {
  return jsx(tag, {
    ...rest,
  }, children)
}

Paragraph.propTypes = {
  tag: PropTypes.string,
  styleType: PropTypes.string
}

Paragraph.defaultProps = {
  tag: "p",
  styleType: "text"
}
