/** @jsx jsx */
import styled from "@emotion/styled";
import colors from "../../styles/colors";

const Button = styled.button`
  display: inline-flex;
  justify-content: center;
  padding: 8px 12px;
  white-space: nowrap;
  cursor: pointer;
  color: ${(props) => props.theme === "light" ? colors.white : colors.mineShaft};
  background-color: transparent;
  border-style: solid;
  border-width: ${(props) => props.border ? `${props.border}px` : "1px"};
  border-color: ${(props) => props.theme === "light" ? colors.white : colors.mineShaft};

  &:focus {
    outline: none;
  }
`;

export default Button;
