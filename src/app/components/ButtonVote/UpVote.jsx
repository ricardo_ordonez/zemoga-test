/** @jsx jsx */
import { jsx } from "@emotion/core";

// Assets
import upVoteIcon from "../../assets/icons/upvote.svg";

// Styles
import * as styles from "./ButtonVote.styles";

export default function UpVote({children, ...rest}) {
  return (
    <button {...rest} css={[styles.button, styles.upVote]}>
      <img src={upVoteIcon} alt="Up vote" />
    </button>
  )
}
