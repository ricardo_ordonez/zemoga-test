/** @jsx jsx */
import { jsx } from "@emotion/core";

// Assets
import downVoteIcon from "../../assets/icons/downvote.svg"

// Styles
import * as styles from "./ButtonVote.styles";

export default function DownVote({children, ...rest}) {
  return (
    <button {...rest} css={[styles.button, styles.downVote]}>
      <img src={downVoteIcon} alt="Down vote" />
    </button>
  )
}
