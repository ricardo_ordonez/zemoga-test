import { css } from "@emotion/core";
import colors from "../../styles/colors";

export const button = css`
  display: inline-flex;
  padding: 10px;
  border: 0;
  color: ${colors.white};
  cursor: pointer;
`;

export const upVote = css`
  background-color: ${colors.java};
`;

export const downVote = css`
  background-color: ${colors.mySin};
`;
