/** @jsx jsx */
import { jsx } from "@emotion/core";
import PropTypes from "prop-types";

// Components
import UpVoteButton from "./UpVote";
import DownVoteButton from "./DownVote";

export default function ButtonVote({voteType, ...rest}) {
  return voteType === "upvote" ? <UpVoteButton {...rest} /> : <DownVoteButton {...rest} />
}

ButtonVote.propTypes = {
  voteType: PropTypes.string
}

ButtonVote.defaultProps = {
  voteType: "upvote"
}
