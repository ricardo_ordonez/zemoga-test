const colors = {
  mySin: "#ffad1d",
  java: "#1cbbb4",
  mineShaft: "#333333",
  white: "#ffffff",
  black: "#000000",
  doveGray: "#707070",
  tundora: "#464646",
  dustyGray: "#979797",
  gallery: "#ebebeb"
}

export default colors;
