const routes = {
  root: '/',
  home: '/home',
  pastTrials: "/past-trials",
  howItWorks: "/how-it-works",
  login: "/login",
  search: "/search",
  conditions: "/conditions",
  policy: "/policy",
  contactUs: "/contact-us"
}

export default routes;
