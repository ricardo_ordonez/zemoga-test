/** @jsx jsx */
import { jsx } from "@emotion/core";
import { Fragment, useState, useEffect } from "react";
import { NavLink } from "react-router-dom";

// Components
import { Header, Card, Paragraph, Button } from "../components";

// Utils
import routes from "../config/routes";
import { data } from "../lib";

// Assets
import pope from "../assets/images/pope.png";
import facebookIcon from "../assets/icons/facebook.png";
import twitterIcon from "../assets/icons/twitter.png";
import closeIcon from "../assets/icons/close.svg";

// Styles
import * as styles from "./Screen.styles";

export default function Home() {
  const [people, setPeople] = useState([]);

  function vote(voteType, person) {
    const peopleCopy = [...people];
    const personIndex = peopleCopy.indexOf(person);
    const currentPerson = peopleCopy[personIndex];
    const modifiedPerson = {
      ...currentPerson,
      votes: {
        ...currentPerson.votes,
        [voteType]: currentPerson.votes[voteType] + 1
      }
    };
    const newPeople = [
      ...people.slice(0, personIndex),
      modifiedPerson,
      ...people.slice(personIndex + 1)
    ];

    setPeople(newPeople);
    saveDataInLocalStorage(JSON.stringify(newPeople));
  }

  function saveDataInLocalStorage(data) {
    localStorage.setItem("people_data", data);
  }

  function getPeopleFromLocalStorage() {
    const people = JSON.parse(localStorage.getItem("people_data")) || data;

    return people;
  }

  useEffect(() => {
    const people = getPeopleFromLocalStorage();

    setPeople(people);
  }, [])

  return (
    <Fragment>
      <div css={styles.heroWrapper}>
        <Header />

        <div css={styles.heroContent}>
          <Card type="hero" bgImage={pope} />
        </div>
        
        <div css={styles.remainingDaysWrapper}>
          <div className="text">Closing In</div>
          <div className="days">
            <span className="counter">22</span> days
          </div>
        </div>
      </div>

      <div css={styles.container}>
        <section css={styles.infoSection}>
          <div className="info-wrapper">
            <div className="catchword">
              <Paragraph tag="h5">Speak out. Be heard.</Paragraph>
              <Paragraph tag="h3">Be counted</Paragraph>
            </div>

            <Paragraph className="message">
              Rule of Thumb is a crowd sourced court of public opinion where
              anyone and everyone can speak out and speak freely.
              It’s easy: You share your opinion, we analyze and put the data in a
              public report.
            </Paragraph>
          </div>

          <button>
            <img src={closeIcon} alt="Close" />
          </button>
        </section>

        <section css={styles.rulingsSection}>
          <Paragraph tag="h1" className="title">Previous Rulings</Paragraph>

         <div css={styles.rulingsList}>
          {
            people.map(person => (
              <Card key={person.id} person={person} vote={vote} />
            ))
          }
         </div>
        </section>

        <section css={styles.callToAction}>
          <Paragraph className="call-text">Is there anyone else you would want us to add?</Paragraph>
          <Button border="2">Submit a Name</Button>
        </section>

        <footer css={styles.footer}>
          <nav>
            <ul>
              <li>
                <NavLink to={routes.conditions} target="_blank">Terms and Conditions</NavLink>
              </li>
              <li>
                <NavLink to={routes.policy} target="_blank">Privacy Policy</NavLink>
              </li>
              <li>
                <NavLink to={routes.contactUs} target="_blank">Contact Us</NavLink>
              </li>
            </ul>
          </nav>

          <div className="social">
            Follow Us
            <a href="https://www.facebook.com/Zemoga?fref=ts" target="_blank">
              <img src={facebookIcon} alt="Facebook" />
            </a>

            <a href="https://twitter.com/Zemoga" target="_blank">
              <img src={twitterIcon} alt="Twitter" />
            </a>
          </div>
        </footer>
      </div>
    </Fragment>
  )
}
