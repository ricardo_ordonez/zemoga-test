import { css } from "@emotion/core";
import { hexToRgb } from "../lib";
import colors from "../styles/colors";

// Assets
import pope from "../assets/images/pope.png";
import callToActionImage from "../assets/images/call-to-action.png";

export const heroWrapper = css`
  position: relative;
  display: flex;
  flex-direction: column;
  width: 100%;
  height: 750px;
  margin-bottom: 30px;

  &::before {
    content: "";
    position: absolute;
    width: 100%;
    height: 100%;
    background-image: url(${pope});
    background-size: cover;
    background-repeat: no-repeat;
    z-index: -1;
  }

  @media screen and (max-width: 767px) {
    height: 850px;
  }
`;

export const heroContent = css`
  flex: 1;
  padding: 0 130px;

  @media screen and (max-width: 767px) {
    .card {
      margin: auto;
    }
  }
  @media screen and (max-width: 992px) {
    padding: 0 15px;
  }
`;

export const remainingDaysWrapper = css`
  display: flex;
  width: 100%;
  height: 50px;

  .text {
    position: relative;
    display: flex;
    align-items: center;
    justify-content: flex-end;
    width: 30%;
    font-size: 13px;
    color: ${colors.white};
    padding: 10px;
    text-transform: uppercase;
    background-color: rgba(${hexToRgb(colors.doveGray)}, 0.3);

    &::after {
      content: "";
      position: absolute;
      top: 50%;
      transform: translateY(-50%);
      width: 0;
      height: 0;
      border-top: 8PX solid transparent;
      border-bottom: 8PX solid transparent;
      border-left: 9px solid ${colors.doveGray};
      right: -9px;
      z-index: -1;
    }
  }

  .days {
    display: flex;
    align-items: center;
    width: 70%;
    font-size: 36px;
    font-weight: 300;
    padding: 10px 20px;
    background-color: rgba(${hexToRgb(colors.white)}, 0.7);

    .counter {
      font-weight: 400;
      margin-right: 6px;
    }
  }
`;

export const container = css`
  max-width: 1024px;
  padding: 0 15px;
  margin: 0 auto;
`;

export const infoSection = css`
  display: flex;
  align-items: center;
  background-color: ${colors.gallery};
  padding: 25px;
  margin-bottom: 40px;

  .info-wrapper {
    display: inline-flex;
    align-items: center;
  }

  .catchword {
    margin-right: 20px;

    h5 {
      font-size: 20px;
      font-weight: 300;
      margin: 0;
    }

    h3 {
      font-size: 34px;
      margin: 0;
    }
  }

  .message {
    flex: 1;
    font-size: 15px;
    font-weight: 300;
    margin: 0;
  }

  button {
    display: inline-flex;
    border: 0;
    cursor: pointer;
    background-color: transparent;
    border: 0;
    padding: 10px;
    margin-left: 20px;
  }

  @media screen and (max-width: 767px) {
    align-items: flex-start;

    .info-wrapper {
      flex-direction: column;
      align-items: flex-start;
    }
  }
`;


export const rulingsSection = css`
  .title {
    font-size: 40px;
    font-weight: 300;
    margin: 0;
    margin-bottom: 27px;
  }
`;

export const rulingsList = css`
  display: grid;
  grid-template-columns: 1fr 1fr;
  gap: 40px;

  @media screen and (max-width: 991px) {
    grid-template-columns: 1fr;

    .card {
      margin: auto;
    }
  }
`;


export const callToAction = css`
  position: relative;
  display: flex;
  justify-content: space-between;
  align-items: center;
  height: 80px;
  padding: 10px 20px;
  margin-top: 30px;

  &::before,
  &::after {
    content: "";
    position: absolute;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    z-index: -1;
  }

  &::before {
    background-image: url(${callToActionImage});
    background-size: cover;
    background-repeat: no-repeat;
    background-position: center;
    z-index: -1;
  }

  &::after {
    background-color: rgba(${hexToRgb(colors.white)}, 0.7)
  }

  .call-text {
    font-size: 30px;
    font-weight: 300;
    margin: 0;
  }

  @media screen and (max-width: 767px) {
    flex-direction: column;
    align-items: flex-start;
    height: auto;
  }
`;

export const footer = css`
  display: flex;
  align-items: center;
  justify-content: space-between;
  border-top: 1px dashed ${colors.dustyGray};
  padding-top: 32px;
  padding-bottom: 40px;
  margin-top: 40px;

  ul {
    display: inline-flex;

    li {
      &:not(:last-of-type) {
        margin-right: 27px;
      }
    }

    a {
      font-size: 12px;
      font-weight: 300;
      color: ${colors.mineShaft};

      &:hover {
        text-decoration: underline;
      }
    }
  }

  .social {
    display: inline-flex;
    align-items: center;
    a {
      margin-left: 22px;
    }
  }

  @media screen and (max-width: 767px) {
    flex-direction: column;
    
    .social {
      margin-top: 20px;
    }
  }
`;
