// Utils
import dayjs from "dayjs";

// Assets

import image1 from "../assets/images/1.png";
import image2 from "../assets/images/2.png";
import image3 from "../assets/images/3.png";
import image4 from "../assets/images/4.png";

export const data = [{
  id: 1,
  name: 'Kanye West',
  summary: 'Vestibulum diam ante, porttitor a odio eget, rhoncus neque. Aenean eu velit libero.',
  avatar: image2,
  area: 'Entertainment',
  time: dayjs("2019-03-15"),
  votes: {
    up: 64,
    down: 36,
  }
},
{
  id: 2,
  name: 'Mark Zuckerberg',
  summary: 'Vestibulum diam ante, porttitor a odio eget, rhoncus neque. Aenean eu velit libero.',
  avatar: image4,
  area: 'Bussines',
  time: dayjs("2020-02-15"),
  votes: {
    up: 36,
    down: 64,
  }
},
{
  id: 3,
  name: 'Cristina Fernández de Kirchner',
  summary: 'Vestibulum diam ante, porttitor a odio eget, rhoncus neque. Aenean eu velit libero.',
  avatar: image1,
  area: 'Politics',
  time: dayjs("2018-04-10"),
  votes: {
    up: 36,
    down: 64,
  }
},
{
  id: 4,
  name: 'Malala Yousafzai',
  summary: 'Vestibulum diam ante, porttitor a odio eget, rhoncus neque. Aenean eu velit libero.',
  avatar: image3,
  area: 'Entertainment',
  time: dayjs("2019-11-15"),
  votes: {
    up: 64,
    down: 36,
  }
}];
