/** @jsx jsx */
import { jsx, Global } from "@emotion/core";
import { Fragment } from "react";
import ReactDOM from "react-dom";

// Components
import Root from "./app/Root";

// Styles
import "./reset.css";
import globalStyles from "./index.styles";
import { normalize } from "react-style-reset";

// Font
import "typeface-lato";

function App() {
  return (
    <Fragment>
      <Global styles={(normalize, globalStyles)} />
      <Root />
    </Fragment>
  )
}

const root = document.querySelector("#root");

ReactDOM.render(<App />, root);
