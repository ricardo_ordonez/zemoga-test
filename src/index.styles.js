import { css } from "@emotion/core";
import colors from "./app/styles/colors";

const globalStyles = css`
  *,
  *::after,
  *::before {
    box-sizing: border-box;
  }

  body {
    height: 100%;
    min-height: 100%;
    margin: 0;
    padding: 0;
    color: ${colors.mineShaft};
    background-color: ${colors.white};
    font-family: 'Lato', sans-serif;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
  }

  ul {
    padding: 0;
    margin: 0;

    li {
      list-style-type: none;

      a {
        text-decoration: none;
      }
    }
  }

`;

export default globalStyles;
